import type { Config } from 'jest';

import defaultConfig from './jest.config';

const config: Config = {
  ...defaultConfig,
  displayName: 'integration-test',
  roots: ['<rootDir>/tests/e2e'],
  testPathIgnorePatterns: ['/unit/'],
};

export default config;
