export function reverse(target: any, propertyKey: string, descriptor: PropertyDescriptor) {

  const originValue = descriptor.value;


  descriptor.value = function (arg: string) {
    const argList = arg.split('').reverse().join('');
    originValue.apply(this, [argList])
  }
}