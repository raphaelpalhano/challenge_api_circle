import express from 'express';
import usersRoute from './routers/users.route';

const app = express();
const PORT = 3000;

app.use(express.json())

app.use(usersRoute);


app.listen(PORT, () => {
  console.log(`App started port ${PORT}`);
});


export default app;