import { Request, Response, Router } from "express";



  const userRoutes = Router();
  const users =  [
      {
        "id": '8e6ee69c-b2d1-4c37-bb00-7e4d2dc1d7ff',
        "name": 'Rafa',
        "email": 'rafa13@gmail.com'
      }
    ]
  

userRoutes.get('/users', (req: Request, res: Response) => {
  return res.status(200).json(users)
})

export default userRoutes;
