import request from 'supertest';
import app from '../../src';

describe('Given I have users router', () => {

  it('get users', async () => {
    //Arrange:
    const expectedResponse = ( [
      {
        "id": '8e6ee69c-b2d1-4c37-bb00-7e4d2dc1d7ff',
        "name": 'Rafa',
        "email": 'rafa13@gmail.com'
      }
    ]
  )
    //Action
    const reponseBody = await request(app).get('/users')
    .set('Accept', 'application/json');

    //assert
    expect(reponseBody.status).toEqual(200);
    expect(reponseBody.body).toEqual(expectedResponse);

  })
})